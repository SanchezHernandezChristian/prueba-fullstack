<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class FlujosComponentes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('flujos_componentes', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('flujos_id');
            $table->foreign('flujos_id')->references('id')->on('flujos')->onUpdate('cascade')
            ->onDelete('cascade');
            $table->integer('componente');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('flujosComponentes');
    }
}
