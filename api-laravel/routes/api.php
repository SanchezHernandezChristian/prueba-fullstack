<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Public 
Route::resource('/flujo', FlujosController::class);
Route::post('/generarToken', [App\Http\Controllers\FlujosController::class, 'login']);
Route::get('/obtenerIdComponente', [App\Http\Controllers\FlujosController::class, 'obtenerFlujoComponente']);
Route::get('/mostrarComponentes', [App\Http\Controllers\FlujosController::class, 'mostrarComponentes']);

//Private
Route::group(['middleware' => ['auth:sanctum']], function () {
    Route::get('/flujo/buscar/{uuid}', [App\Http\Controllers\FlujosController::class, 'search']);
    Route::post('/guardarComponente', [App\Http\Controllers\FlujosController::class, 'guardarComponentes']);
});