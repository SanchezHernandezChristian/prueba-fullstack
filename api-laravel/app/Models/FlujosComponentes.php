<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class flujosComponentes extends Model
{
    use HasFactory;
    
    protected $fillable = [
        'componente',
        'flujos_id',
        'id',
        
    ];

}
