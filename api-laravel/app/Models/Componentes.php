<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class Componentes extends Authenticatable 
{
    use HasFactory, Notifiable, HasApiTokens;
    protected $fillable = [
        'flujoComponente_id',
        'componente',
        'datos',
    ];

}
