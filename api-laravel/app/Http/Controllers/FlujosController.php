<?php

namespace App\Http\Controllers;

use App\Models\Flujo;
use App\Models\FlujosComponentes;
use App\Models\Componentes;
use Illuminate\Http\Request;
use Uuid;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Support\Facades\Validator;

class FlujosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $flujos =  Flujo::all();
        return $flujos; //= Uuid::generate()->string;;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request -> validate([
            'usuario' => 'required',
            'componente' => 'required',
        ]);

        $flujo = Flujo::create($request -> all());
        $array = explode(",", $request -> componente);
        $array_length = count($array);
         for ($i = 0; $i < $array_length; ++$i) {
            $flujoComponente = FlujosComponentes::create(['componente' => $array[$i] ] + ['flujos_id' => $flujo->id]);
            }
        $flujo->uuid = Uuid::generate()->string;
        $flujo->save();
        return $flujo;  
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Flujo  $flujo
     * @return \Illuminate\Http\Response
     */
    public function show(Flujo $flujo)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Flujo  $flujo
     * @return \Illuminate\Http\Response
     */
    public function edit(Flujo $flujo)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Flujo  $flujo
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $uuid)
    {   
        $request -> validate([
            'uuid' => 'required',
            'componente' => 'required',
        ]);
        $flujo = Flujo::where('uuid', $uuid)->first();
        $flujo->update($request->all());
        FlujosComponentes::where('flujos_id', $flujo->id)->delete();
        $array = explode(",", $request -> componente);
        $array_length = count($array);
         for ($i = 0; $i < $array_length; ++$i) {
            $flujoComponente = FlujosComponentes::create(['componente' => $array[$i] ] + ['flujos_id' => $flujo->id]);
            }
        return $flujo;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Flujo  $flujo
     * @return \Illuminate\Http\Response
     */
    public function destroy($uuid)
    {
        Flujo::where('uuid', $uuid)->delete();
    }

    public function search($uuid)
    {
        try {
            $flujo = Flujo::where('uuid', $uuid) -> get() -> first();
                $flujoComponente = FlujosComponentes::where('flujos_id', $flujo->id) -> get();
                return $flujoComponente;
        }
            catch (exception $e) {
                return back()->withError($exception->getMessage())->withInput();
            }
    }

    public function login(Request $request) {
        $fields = $request->validate([
            'uuid' => 'required|string'
        ]);

        // Check uuid
        if(!$uuid = Flujo::where('uuid', $fields['uuid'])->first()) {
            return response([
                'message' => 'Bad creds',
                'uuid' => $uuid,
            ], 401);
        }

        $token = $uuid->createToken('myapptoken')->plainTextToken;

        $response = [
            'uuid' => $uuid->uuid,
            'token' => $token
        ];

        return response($response, 201);
    }

    public function obtenerFlujoComponente(Request $request){
         $fields = $request->validate([
            'uuid' => 'required|string',
            'componente' => 'required'
        ]);
        $flujo = Flujo::select('flujos_componentes.id')
                ->join('flujos_componentes','flujos.id', '=', 'flujos_componentes.flujos_id')
                ->where('flujos.uuid', $request->uuid)
                ->where('flujos_componentes.componente', $request->componente)
                ->get();

                return $flujo;
    }

    public function guardarComponentes(Request $request){
        $fields = $request->validate([
            'flujoComponente_id' => 'required',
            'componente' => 'required',
            'datos' => 'required'
        ]);

        $componente = Componentes::updateOrCreate(
            ['flujoComponente_id' => $request -> flujoComponente_id, 'componente' => $request -> componente,],
            ['datos' => $request -> datos]);
        return $componente;
    } 

    public function mostrarComponentes(Request $request){
         $fields = $request->validate([
            'uuid' => 'required|string'
        ]);

        $flujo = Flujo::select('*')
                ->join('flujos_componentes','flujos.id', '=', 'flujos_componentes.flujos_id')
                ->join('componentes', 'flujos_componentes.id', '=', 'componentes.flujoComponente_id')
                ->where('flujos.uuid', $request->uuid)
                ->get();

                return $flujo;

    }

}
