import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import vuetify from "./plugins/vuetify";
import VueSignature from "vue-signature-pad";

Vue.use(VueSignature);

Vue.config.productionTip = false;

Vue.mixin({
  data: function() {
    return {
      get API_URL() {
        //return "http://localhost:8001";
        return "https://laravel-api-312705.nn.r.appspot.com";
      },
    };
  },
});

new Vue({
  router,
  store,
  vuetify,
  render: (h) => h(App),
}).$mount("#app");
