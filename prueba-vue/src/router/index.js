import Vue from "vue";
import VueRouter from "vue-router";
import Home from "../views/Home.vue";
import Flujo from "../views/Flujo.vue";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "Home",
    component: Home,
  },
  {
    path: "/:uuid",
    name: "Flujo",
    component: Flujo,
  },
];

const router = new VueRouter({
  mode: "history",
  routes,
});

export default router;
